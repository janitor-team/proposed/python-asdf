Source: python-asdf
Maintainer: Debian Astronomy Team <debian-astro-maintainers@lists.alioth.debian.org>
Uploaders: Ole Streicher <olebole@debian.org>
Section: python
Priority: optional
Build-Depends: debhelper-compat (= 13),
               dh-python,
               graphviz,
               python3-all,
               python3-astropy,
               python3-jsonschema,
               python3-lz4 (>=0.10),
               python3-pytest (>=2.7.2),
               python3-pytest-astropy,
               python3-semantic-version (>=2.8),
               python3-setuptools,
               python3-setuptools-scm,
               python3-sphinx,
               python3-sphinx-astropy,
               python3-yaml
Standards-Version: 4.5.0
Vcs-Browser: https://salsa.debian.org/debian-astro-team/python-asdf
Vcs-Git: https://salsa.debian.org/debian-astro-team/python-asdf.git
Homepage: https://github.com/spacetelescope/asdf
Rules-Requires-Root: no

Package: python3-asdf
Architecture: all
Depends: ${misc:Depends},
         ${python3:Depends}
Description: Python 3 library for the Advanced Scientific Data Format
 ASDF (Advanced Scientific Data Format) is a proposed
 next generation interchange format for scientific data. ASDF aims to
 exist in the same middle ground that made FITS so successful, by
 being a hybrid text and binary format: containing human editable
 metadata for interchange, and raw binary data that is fast to load
 and use. Unlike FITS, the metadata is highly structured and is
 designed up-front for extensibility.
 .
 This is the Python 3 package.

Package: python-asdf-doc
Architecture: all
Multi-Arch: foreign
Section: doc
Depends: ${misc:Depends},
         ${sphinxdoc:Depends},
         libjs-mathjax
Description: Python library for the Advanced Scientific Data Format (documentation)
 ASDF (Advanced Scientific Data Format) is a proposed
 next generation interchange format for scientific data. ASDF aims to
 exist in the same middle ground that made FITS so successful, by
 being a hybrid text and binary format: containing human editable
 metadata for interchange, and raw binary data that is fast to load
 and use. Unlike FITS, the metadata is highly structured and is
 designed up-front for extensibility.
 .
 This package contains the API documentation.

Package: asdftool
Architecture: all
Section: science
Depends: python3,
         python3-asdf,
         ${misc:Depends}
Description: Command line tool to manipulate ASDF scientific data files
 ASDF (Advanced Scientific Data Format) is a proposed
 next generation interchange format for scientific data. ASDF aims to
 exist in the same middle ground that made FITS so successful, by
 being a hybrid text and binary format: containing human editable
 metadata for interchange, and raw binary data that is fast to load
 and use. Unlike FITS, the metadata is highly structured and is
 designed up-front for extensibility.
 .
 This package contains the asdf command line tool.
